﻿#include <iostream>
#include <string>

int main()
{
    std::string str = "Hello Skillbox!";

    std::cout << str << "\n" << "Length: " << str.length() << "\n" << "First element: "
        << str[0] << "\n" << "Last element: " << str[str.length() - 1];

    return 0;
}